# spiralbot
# Copyright 2019, Luna Mendes and spiralbot contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import time
import tempfile
import subprocess

from random import randint
from pathlib import Path
from configparser import ConfigParser

import requests

from mastodon import Mastodon

log = logging.getLogger(__name__)

# stores the last unix timestamp the bot posted on
LAST_POST_FILE = Path.cwd() / '.last_post'

def color_pair() -> tuple:
    """Generate a color pair from randoma11y.com"""
    resp = requests.get('https://randoma11y.com/stats')
    rjson = resp.json()

    color_url = (
        'https://randoma11y.com/combos?page='
        f'{randint(1, rjson["combos"])}'
        '&per_page=1'
    )

    resp = requests.get(color_url)

    if resp.status != 200:
        raise Exception('Failed to contact randoma11y')

    color = resp.json()[0]
    color1, color2 = color['color_one'], color['color_two']

    # TODO: return ratio too
    return color1.upper(), color2.upper()


def to_hex(tup) -> str:
    """Convert rgb tuple to a hex value"""
    return '#%02x%02x%02x' % tup


def _valid(num):
    return 0 <= num <= 255


def _multi(color_r, color_g, color_b) -> list:
    """Generate color variatons based on the given RGB tuple."""

    # next 8 are the main mix
    res = [(color_r - x, color_g - x, color_b - x) for x in range(8)] + \
          [(color_r - x, color_g - x, color_b + x) for x in range(8)] + \
          [(color_r - x, color_g + x, color_b - x) for x in range(8)] + \
          [(color_r - x, color_g + x, color_b + x) for x in range(8)] + \
          [(color_r + x, color_g - x, color_b - x) for x in range(8)] + \
          [(color_r + x, color_g - x, color_b + x) for x in range(8)] + \
          [(color_r + x, color_g + x, color_b - x) for x in range(8)] + \
          [(color_r + x, color_g + x, color_b + x) for x in range(8)] + \
          [(color_r - x, color_g, color_b) for x in range(4)] + \
          [(color_r, color_g - x, color_b) for x in range(4)] + \
          [(color_r, color_g, color_b - x) for x in range(4)] + \
          [(color_r + x, color_g, color_b) for x in range(4)] + \
          [(color_r, color_g + x, color_b) for x in range(4)] + \
          [(color_r, color_g, color_b + x) for x in range(4)]

    # filter invalid colors (with components above 255 or below 0)
    # and make it all unique via set().
    return set(filter(
        lambda tup: _valid(tup[0]) and _valid(tup[1]) and _valid(tup[2]),
        res
    ))


def _append_color_transform(lines, tup, target):
    multi_res = _multi(*tup)
    for col in multi_res:
        hex_val = to_hex(col).upper()

        lines.append(
            f'--change-color "{hex_val}" "{target}" '
        )


def _changes(pair: tuple) -> str:
    # yes, ugly solution
    lines = [
        'gifsicle',
        f'--change-color "#ffffff" "{pair[0]}"',
        f'--change-color "#000000" "{pair[1]}"'
    ]

    # light transforms

    # the given colors are based off the spiral.gif file i operate with
    # spiralbot. if you're using a different one, those values may vary wildly.

    _append_color_transform(lines, (255, 255, 255), pair[0])
    _append_color_transform(lines, (252, 255, 252), pair[0])
    _append_color_transform(lines, (228, 228, 228), pair[0])
    _append_color_transform(lines, (251, 253, 253), pair[0])

    _append_color_transform(lines, (168, 167, 168), pair[0])
    _append_color_transform(lines, (165, 169, 165), pair[0])
    _append_color_transform(lines, (152, 152, 152), pair[0])
    _append_color_transform(lines, (209, 209, 209), pair[0])
    _append_color_transform(lines, (239, 244, 239), pair[0])

    _append_color_transform(lines, (253, 253, 251), pair[0])
    _append_color_transform(lines, (246, 250, 246), pair[0])
    _append_color_transform(lines, (220, 226, 220), pair[0])
    _append_color_transform(lines, (249, 251, 249), pair[0])
    _append_color_transform(lines, (81, 81, 81), pair[0])

    _append_color_transform(lines, (185, 185, 185), pair[0])
    _append_color_transform(lines, (140, 144, 140), pair[0])
    _append_color_transform(lines, (91, 90, 91), pair[0])
    _append_color_transform(lines, (176, 180, 176), pair[0])

    _append_color_transform(lines, (244, 244, 244), pair[0])
    _append_color_transform(lines, (184, 179, 183), pair[0])
    _append_color_transform(lines, (196, 196, 196), pair[0])
    _append_color_transform(lines, (253, 251, 253), pair[0])
    _append_color_transform(lines, (241, 239, 241), pair[0])

    _append_color_transform(lines, (135, 135, 135), pair[0])
    _append_color_transform(lines, (107, 106, 107), pair[0])
    _append_color_transform(lines, (237, 240, 237), pair[0])
    _append_color_transform(lines, (169, 169, 169), pair[0])

    _append_color_transform(lines, (236, 236, 236), pair[0])
    _append_color_transform(lines, (220, 220, 220), pair[0])
    _append_color_transform(lines, (177, 177, 177), pair[0])

    # dark transforms

    _append_color_transform(lines, (0, 0, 0), pair[1])
    _append_color_transform(lines, (1, 0, 1), pair[1])

    _append_color_transform(lines, (22, 22, 22), pair[1])
    _append_color_transform(lines, (27, 27, 27), pair[1])

    _append_color_transform(lines, (13, 15, 13), pair[1])
    _append_color_transform(lines, (49, 49, 49), pair[1])

    _append_color_transform(lines, (38, 38, 38), pair[1])
    _append_color_transform(lines, (70, 70, 70), pair[1])
    _append_color_transform(lines, (17, 12, 16), pair[1])
    _append_color_transform(lines, (61, 62, 61), pair[1])
    _append_color_transform(lines, (59, 59, 59), pair[1])
    _append_color_transform(lines, (119, 119, 119), pair[1])
    _append_color_transform(lines, (125, 125, 125), pair[1])
    _append_color_transform(lines, (97, 98, 97), pair[1])

    _append_color_transform(lines, (44, 43, 43), pair[1])
    _append_color_transform(lines, (12, 12, 12), pair[1])
    _append_color_transform(lines, (16, 17, 18), pair[1])

    _append_color_transform(lines, (9, 8, 9), pair[1])

    return lines


def gen_spiral(pair: tuple) -> str:
    """Generate a spiral. Outputs the path of the generated gif."""
    _fd, out_path = tempfile.mkstemp(suffix='.gif')

    command_lines = _changes(pair)
    command_lines.append(f'./spiral.gif > {out_path}')
    command = ' '.join(command_lines)

    log.debug('gif command: %s', command)

    subprocess.check_output(
        command,
        stderr=subprocess.PIPE,
        shell=True
    )

    return out_path


def post_spiral(mastodon):
    """Post a spiral."""
    now = int(time.time())
    log.info('posting a spiral at %d', now)

    try:
        pair = color_pair()
    except Exception as e:
        log.error('error while getting color pair: %r', e)
        LAST_POST_FILE.write_text(str(now))
        return

    outpath = gen_spiral(pair)

    media = mastodon.media_post(outpath)

    mastodon.status_post(
        f'spiral with colors {pair[0].lstrip("#")} and {pair[1].lstrip("#")}',
        media_ids=[media['id']], sensitive=True,
        visibility='unlisted'
    )

    LAST_POST_FILE.write_text(str(now))


def main_loop(mastodon):
    """Enter main loop waiting for the next time to post."""
    post_period = int(mastodon.cfg['post_period'])

    while True:
        last_post = int(LAST_POST_FILE.read_text())
        next_ts = last_post + post_period

        now = time.time()

        log.debug('now: %.2f next_ts: %.2f',
                  now, next_ts)

        if now > next_ts:
            post_spiral(mastodon)

        time.sleep(60)


def main():
    """Main entrypoint"""
    cfg = ConfigParser()
    cfg.read('./config.ini')

    cwd = Path.cwd()
    bot_cfg = cfg['spiralbot']

    if bot_cfg['debug']:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    usercred_path = cwd / 'spiralbot_appcred.secret'

    if not usercred_path.exists():
        log.info('usercred.secret does not exist, creating')

        Mastodon.create_app(
            'spiralbot',
            api_base_url=bot_cfg['instance'],
            to_file=usercred_path
        )

    log.info('instantiating client')
    mastodon = Mastodon(
        api_base_url=bot_cfg['instance'],
        client_id='spiralbot_appcred.secret'
    )

    # inject config into the client
    mastodon.cfg = bot_cfg

    mastodon.log_in(
        bot_cfg['user'],
        bot_cfg['password'],
        to_file='spiralbot_usercred.secret'
    )

    if not LAST_POST_FILE.exists():
        log.info('creating last post file')
        LAST_POST_FILE.write_text('0')

    main_loop(mastodon)

if __name__ == '__main__':
    main()
