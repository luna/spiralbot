# spiralbot

nobody can stop me

## requirements

 - python 3.6
 - pipenv
 - gifsicle
 - a spiral gif that has `#ffffff` and `#000000` as main colors, save it at
    `./spiral.gif` 

## install

```
git clone https://gitlab.com/luna/spiralbot.git
cd spiralbot
cp config.example.ini config.ini

# edit config.ini

pipenv install
```

## run

```
pipenv run python bot.py
```
